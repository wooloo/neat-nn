# Neat NN

A pure-rust neural network library made with a very superficial knowledge of neural networks.

It implements:
* Layer types
  * Addition
  * Multiplication
  * Mean
* Optimization types
  * Gradient descent  
    Will get stuck at local maxima, use of multiple starting points is left up to the end user.
