#![allow(unused_variables)]
#![allow(unused_assignments)]

use neat_nn::{Layer, LayerState, Network};
#[test]
fn simple_learn_return_zero() {
	// Define the layers of the network
	let layers = vec![Layer::new(1), Layer::new(1)];
	// Build the network (actually only stores the weights of the network, but that's not important)
	let mut starting_network = Network::new(layers);
	// Randomize the new network's weights to give it a fresh starting point
	starting_network.randomize(100.0);
	println!("{:?}", starting_network);
	// Run the gradient descent algorithm with...
	// 1000 randomly sampled neighbors for each position,
	// 1 try for each neighbor (it's deterministic),
	// an initial step distance of 10,
	// a minimal cardinal distance from the optimal position of 0.1,
	// and an evaluator function that...
	let finished_network = starting_network.descend(1000, 1, 10.0, 0.1, &|net: &Network| -> f64 {
		// Builds an input layer with 100.0,
		let input = 100.0;
		let mut input_layer = LayerState::new(1);
		input_layer.values[0] = input;
		// Runs the network,
		let output_layer = net.evaluate(input_layer);
		// And gives a score depending on the result's distance from zero.
		100.0 - (output_layer.values[0]).abs()
	});
	println!("{:?}", finished_network)
}

#[test]
fn simple_learn_multiply() {
	// Define the layers of the network
	let layers = vec![Layer::new(2), Layer::new(1)];
	// Keep track of the best network so far.
	let mut best = Network::new(layers.clone());
	let mut best_score = f64::MIN;
	// Keep training until we score > -0.01.
	while best_score < -0.01 {
		// Generate a new starting point
		let mut starting_network = Network::new(layers.clone());
		starting_network.randomize(2.0);
		// Run gradient descent with...
		// 1000 neighbors,
		// 100 tries for each neighbor,
		// A starting distance of 2.0,
		// A minimum distance of 0.1,
		// And an evaluator that...
		let finished_network = starting_network.descend(1000, 100, 2.0, 0.1, &|net| -> f64 {
			// Picks two factors
			let factor_a = rand::random::<f64>() * 10.0;
			let factor_b = rand::random::<f64>() * 10.0;
			// Supplies the input layer
			let mut input_layer = LayerState::new(2);
			input_layer.values = vec![factor_a, factor_b];
			// Runs the network
			let output_layer = net.evaluate(input_layer);
			// Checks how close the result was
			let score = 0.0 - (output_layer.values[0] - (factor_a * factor_b)).abs();
			score
		});
		// Check if this starting point is better than the previous best
		if finished_network.1 > best_score {
			best = finished_network.0;
			best_score = finished_network.1;
		}
	}
}
