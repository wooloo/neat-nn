use rand::{prelude::SliceRandom, thread_rng};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

#[derive(Clone, Debug, EnumIter)]
pub enum Operation {
	Add,
	Multiply,
	Mean,
}

#[derive(Clone, Debug)]
pub struct Network {
	pub weights: Vec<Weights>,
}

impl Network {
	pub fn evaluate(&self, input: LayerState) -> LayerState {
		self.weights.iter().fold(input, |state, next_weights| {
			debug_assert_eq!(state.values.len(), next_weights.input_count);
			state.apply(next_weights)
		})
	}
	pub fn new(layers: Vec<Layer>) -> Network {
		Network {
			weights: Weights::build_for(layers),
		}
	}
	pub fn all_neighbors(&self, distance: f64) -> Vec<Network> {
		let mut out = vec![];
		for (index, weight) in self.weights.iter().enumerate() {
			for neighbor in weight.neighbors(distance) {
				let mut new = self.clone();
				new.weights[index] = neighbor;
				out.push(new);
			}
		}
		out
	}
	pub fn sample_neighbors(&self, distance: f64, count: usize) -> Vec<Network> {
		let mut out = vec![];
		for _ in 0..count {
			let mut new = self.clone();
			new.randomize(distance);
			out.push(new);
		}
		out
	}
	pub fn randomize(&mut self, distance: f64) {
		for i in &mut self.weights {
			i.weights = i
				.weights
				.iter()
				.map(|v| v + ((rand::random::<f64>() * distance * 2.0) - distance))
				.collect();
			i.operation = {
				let operations = Operation::iter().collect::<Vec<Operation>>();
				operations
					.as_slice()
					.choose(&mut thread_rng())
					.unwrap()
					.clone()
			}
		}
	}
	pub fn descend(
		&mut self,
		neighbor_count: usize,
		tries_count: usize,
		starting_distance: f64,
		min_distance: f64,
		evaluator: &dyn Fn(&Network) -> f64,
	) -> (Network, f64) {
		let mut best = self.clone();
		let mut best_score = evaluator(&best);
		let mut distance = starting_distance;
		while distance > min_distance {
			let mut found_better = false;
			for neighbor in best.sample_neighbors(distance, neighbor_count) {
				let new_score = {
					let mut output = f64::from(0);
					for _ in 0..tries_count {
						output += evaluator(&neighbor);
					}
					output / tries_count as f64
				};
				if new_score > best_score {
					best = neighbor;
					best_score = new_score;
					found_better = true;
				}
			}
			if !found_better {
				distance /= 2.0;
			}
		}
		(best, best_score)
	}
}

#[derive(Clone)]
pub struct Layer {
	neuron_count: usize,
	operation: Operation,
}

impl Layer {
	pub fn new(size: usize) -> Layer {
		Layer {
			neuron_count: size,
			operation: Operation::Add,
		}
	}
}

#[derive(Clone, Debug)]
pub struct Weights {
	input_count: usize,
	output_count: usize,
	weights: Vec<f64>,
	operation: Operation,
}

impl Weights {
	pub fn weight_for(&self, input: usize, output: usize) -> f64 {
		self.weights[(input * self.output_count) + output]
	}
	pub fn build_for(layers: Vec<Layer>) -> Vec<Weights> {
		layers
			.windows(2)
			.map(|v| Weights {
				input_count: v[0].neuron_count,
				output_count: v[1].neuron_count,
				weights: vec_of_zeroes(v[0].neuron_count * v[1].neuron_count),
				operation: v[1].operation.clone(),
			})
			.collect::<Vec<Weights>>()
	}
	pub fn new(
		input_count: usize,
		output_count: usize,
		weights: Vec<f64>,
		operation: Operation,
	) -> Weights {
		Weights {
			input_count,
			output_count,
			weights,
			operation,
		}
	}
	pub fn neighbors(&self, distance: f64) -> Vec<Weights> {
		let mut results = vec![];
		for i in Operation::iter() {
			let mut new = self.clone();
			new.operation = i;
			results.push(new)
		}
		for i in (-1 * self.weights.len() as isize)..=self.weights.len() as isize {
			if i == 0 {
				continue;
			}
			let mut new = self.clone();
			new.weights[(i.abs() as usize) - 1] += if i > 0 { distance } else { -distance };
			results.push(new)
		}
		results
	}
}

pub struct LayerState {
	pub values: Vec<f64>,
}

impl LayerState {
	pub fn new(size: usize) -> LayerState {
		LayerState {
			values: vec_of_zeroes(size),
		}
	}
	pub fn apply(&self, weights: &Weights) -> LayerState {
		let mut result = LayerState::new(weights.output_count);
		for output in 0..weights.output_count {
			match weights.operation {
				Operation::Add => {
					result.values[output] = self
						.values
						.iter()
						.enumerate()
						.map(|(index, input)| input * weights.weight_for(index, output))
						.sum()
				}
				Operation::Multiply => {
					result.values[output] = self
						.values
						.iter()
						.enumerate()
						.map(|(index, input)| input * weights.weight_for(index, output))
						.product()
				}
				Operation::Mean => {
					let result_ = self
						.values
						.iter()
						.enumerate()
						.map(|(index, input)| input * weights.weight_for(index, output))
						.fold((0.0, 0), |state, next| (state.0 + next, state.1 + 1));
					result.values[output] = result_.0 / result_.1 as f64
				}
			}
		}
		result
	}
}

fn vec_of_zeroes<T: From<u32>>(size: usize) -> Vec<T> {
	let mut vec = Vec::with_capacity(size);
	for _ in 0..size {
		vec.push(T::from(0))
	}
	vec
}
